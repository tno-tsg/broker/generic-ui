FROM node:20-alpine as builder

WORKDIR /app
COPY package* /app/
RUN npm install
COPY . .
RUN npm run build

FROM nginx

ADD default.conf.template /etc/nginx/conf.d/default.conf.template

CMD ["/bin/bash", "-c", "envsubst '$BROKER_ACCESSURL' < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/default.conf && exec nginx -g 'daemon off;'"]

COPY --from=builder /app/dist /app
